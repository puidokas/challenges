namespace CounterTests
{
    public class UnitTest
    {
        [Fact]
        public void ProcessFile_ShouldCountWords_CaseSensitive()
        {
            List<string> filePaths = new List<string>()
            {
                Path.GetFullPath(@"data\file1.txt"),
                Path.GetFullPath(@"data\file2.txt"),
            };

            List<(int, string)> data = new List<(int, string)>
            {
                (1, "Go"),
                (2, "do"),
                (2, "that"),
                (1, "thing"),
                (1, "you"),
                (1, "so"),
                (2, "well"),
                (1, "I"),
                (1, "play"),
                (1, "football"),
            };

            var result = Counter.Program.Run(filePaths);

            foreach ((int count, string word) in data)
            {
                Assert.Equal(count, result[word]);
            }
        }
    }
}