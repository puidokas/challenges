﻿using System.Collections.Concurrent;
using System.Text.RegularExpressions;

namespace Counter;

public class Program
{
    private static void Main()
    {
        var files = Directory.EnumerateFiles("data", "*.txt");

        var results = Run(files);

        PrintResults(results);
    }

    public static ConcurrentDictionary<string, int> Run(IEnumerable<string> files)
    {
        ConcurrentDictionary<string, int> wordCounts = new ConcurrentDictionary<string, int>();

        Parallel.ForEach(files, filePath =>
        {
            ProcessFile(filePath, wordCounts);
        });

        return wordCounts;
    }

    private static void ProcessFile(string filePath, ConcurrentDictionary<string, int> wordCounts)
    {
        try
        {
            using StreamReader reader = new StreamReader(filePath);
            string? line;
            
            while ((line = reader.ReadLine()) != null)
            {
                string[] words = Regex.Split(line, @"\W+");

                foreach (string word in words)
                {
                    if (!string.IsNullOrWhiteSpace(word))
                    {
                        wordCounts.AddOrUpdate(word, 1, (_, count) => count + 1);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error processing file {filePath}: {ex.Message}");
        }
    }

    private static void PrintResults(ConcurrentDictionary<string, int> wordCounts)
    {
        Console.WriteLine("Word Counts:");
        foreach (var entry in wordCounts.OrderByDescending(pair => pair.Value))
        {
            Console.WriteLine($"{entry.Value}: {entry.Key}");
        }
    }
}
