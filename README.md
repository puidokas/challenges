# challenges

Coding challenges implemented in various programming languages.

## count_unique_words

Given multiple text files, the program counts the occurrences of each unique word in the files and aggregates the results.
