public static void main(String[] args) {
    SourceSample source = new SourceSample();
    SinkSample sink = new SinkSample();

    EventProcessingComponent component = new EventProcessingComponent(source, sink);
    component.start();

    byte[] salt = "SampleSalt".getBytes();
    byte[] message = "SampleMessage".getBytes();

    source.receiveSalt(salt);

    source.receiveMessage(1, message);
}
