public class EventProcessingComponent extends Solution {

    public EventProcessingComponent(Source source, Sink sink) {
        super(source, sink);
    }

    @Override
    protected Observer createObserver(Sink sink) {
        return new HashingObserver(sink);
    }
}
