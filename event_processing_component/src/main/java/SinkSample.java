public class SinkSample implements Sink {

    @Override
    public void publishHash(long id, byte[] message, byte[] salt, byte[] hash) {
        String messageString = new String(message);
        String saltString = new String(salt);
        String hashString = new String(hash);

        System.out.println("Received hash for message with ID " + id + ":");
        System.out.println("Message: " + messageString);
        System.out.println("Salt: " + saltString);
        System.out.println("Hash: " + hashString);
        System.out.println();
    }
}
