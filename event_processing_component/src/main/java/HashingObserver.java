import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class HashingObserver implements Observer {

    private final Sink sink;
    private byte[] salt;

    public HashingObserver(Sink sink) {
        this.sink = sink;
    }

    @Override
    public void onSalt(byte[] salt) {
        this.salt = salt;
    }

    @Override
    public void onMessage(long id, byte[] message) {
        if (salt == null) {
            return;
        }

        byte[] hash = hashMsgWithSalt(message, salt);
        sink.publishHash(id, message, salt, hash);
    }

    private byte[] hashMsgWithSalt(byte[] message, byte[] salt) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] concatenated = concatenate(message, salt);

            // Apply SHA-256 hashing function 5000 times
            for (int i = 0; i < 5000; i++) {
                concatenated = digest.digest(concatenated);
            }
            return concatenated;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }
    }

    private byte[] concatenate(byte[] arr1, byte[] arr2) {
        byte[] result = new byte[arr1.length + arr2.length];
        System.arraycopy(arr1, 0, result, 0, arr1.length);
        System.arraycopy(arr2, 0, result, arr1.length, arr2.length);
        return result;
    }
}
