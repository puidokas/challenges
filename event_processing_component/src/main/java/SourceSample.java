import java.util.ArrayList;
import java.util.List;

public class SourceSample implements Source {
    private List<Observer> observers;

    public SourceSample() {
        this.observers = new ArrayList<>();
    }

    @Override
    public void subscribe(Observer observer) {
        observers.add(observer);
    }

    public void receiveSalt(byte[] salt) {
        for (Observer observer : observers) {
            observer.onSalt(salt);
        }
    }

    public void receiveMessage(long id, byte[] message) {
        for (Observer observer : observers) {
            observer.onMessage(id, message);
        }
    }
}
