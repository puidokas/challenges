package com.puzzle.api.service;

import com.puzzle.api.game.MoveRequest;
import com.puzzle.api.game.PuzzleGame;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Service
public class PuzzleGameService {

    private final Map<String, PuzzleGame> games = new HashMap<>();

    public String createGame() {
        String gameId = UUID.randomUUID().toString();
        PuzzleGame newGame = new PuzzleGame();
        games.put(gameId, newGame);
        return gameId;
    }

    public int[][] getGameState(String gameId) {
        PuzzleGame game = games.get(gameId);
        return game != null ? game.getBoard() : null;
    }

    public boolean moveTile(String gameId, MoveRequest moveRequest) {
        PuzzleGame game = games.get(gameId);
        if (game == null) {
            return false;
        }

        int tileNumber = moveRequest.getTileNumber();
        String direction = moveRequest.getDirection();

        return game.moveTile(tileNumber, direction);
    }
}
