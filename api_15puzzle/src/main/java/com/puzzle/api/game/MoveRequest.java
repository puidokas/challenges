package com.puzzle.api.game;

public class MoveRequest {
    private int tileNumber;
    private String direction;

    public MoveRequest() {
        // Default constructor required for JSON deserialization
    }

    public MoveRequest(int tileNumber, String direction) {
        this.tileNumber = tileNumber;
        this.direction = direction;
    }

    public int getTileNumber() {
        return tileNumber;
    }

    public void setTileNumber(int tileNumber) {
        this.tileNumber = tileNumber;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }
}
