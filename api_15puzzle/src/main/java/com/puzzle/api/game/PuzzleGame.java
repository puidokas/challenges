package com.puzzle.api.game;

public class PuzzleGame {

    private final int boardSize = 4;

    public PuzzleGame() {}

    public int[][] getBoard() {
        return new int[boardSize][boardSize];
    }

    public boolean moveTile(int tileNumber, String direction) {
        System.out.println("Moving tile " + tileNumber + " in direction " + direction);
        return true;
    }

    public boolean isSolved() {
        return false;
    }

    public boolean isValidMove(int tileNumber, String direction) {
        return true;
    }

    // Other methods to interact with the game state
}
