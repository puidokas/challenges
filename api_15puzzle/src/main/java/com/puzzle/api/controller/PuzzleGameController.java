package com.puzzle.api.controller;

import com.puzzle.api.game.MoveRequest;
import com.puzzle.api.service.PuzzleGameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class PuzzleGameController {

    private final PuzzleGameService puzzleGameService;

    @Autowired
    public PuzzleGameController(PuzzleGameService puzzleGameService) {
        this.puzzleGameService = puzzleGameService;
    }

    @PostMapping("/games")
    public ResponseEntity<String> createGame() {
        String gameId = puzzleGameService.createGame();
        return ResponseEntity.ok(gameId);
    }

    @GetMapping("/games/{gameId}")
    public ResponseEntity<int[][]> getGameState(@PathVariable String gameId) {
        int[][] gameState = puzzleGameService.getGameState(gameId);
        if (gameState == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(gameState);
    }

    @PutMapping("/games/{gameId}/move")
    public ResponseEntity<String> moveTile(@PathVariable String gameId, @RequestBody MoveRequest moveRequest) {
        boolean moveResult = puzzleGameService.moveTile(gameId, moveRequest);
        if (moveResult) {
            return ResponseEntity.ok("Tile moved successfully");
        } else {
            return ResponseEntity.badRequest().body("Invalid move");
        }
    }
}
